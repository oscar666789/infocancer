function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-articulos-articulos-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/articulos/articulos.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/articulos/articulos.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesArticulosArticulosPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"inicio\"></ion-back-button>\n    </ion-buttons>\n    <ion-title text-center>Articulos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-header>\n      <ion-card-title>Title</ion-card-title>\n      <ion-card-subtitle>Subtitle</ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content>\n      <p>\n        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum\n        reiciendis aliquam magnam tempora a.\n      </p>\n      <ion-button fill=\"outline\" slot=\"end\">View</ion-button>\n    </ion-card-content>\n  </ion-card>\n  <body><div id='compartir'>\n      <!-- Load Facebook SDK for JavaScript -->\n<div id=\"fb-root\"></div>\n<script async defer src=\"https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6\"></script>\n\n<!-- Your embedded comments code -->\n<div class=\"fb-comment-embed\"\n   data-href=\"https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185\"\n   data-width=\"500\"></div>\n\n  </div>\n    <p>comentarios</p>\n    <div id='comentarios'>\n      <div class=\"fb-comments\" data-href=\"http://facebook.com/infocancerut\" data-numposts=\"5\" data-width=\"\"></div>\n  </div> \n  </body>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/articulos/articulos-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/articulos/articulos-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: ArticulosPageRoutingModule */

  /***/
  function srcAppPagesArticulosArticulosRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArticulosPageRoutingModule", function () {
      return ArticulosPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _articulos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./articulos.page */
    "./src/app/pages/articulos/articulos.page.ts");

    var routes = [{
      path: '',
      component: _articulos_page__WEBPACK_IMPORTED_MODULE_3__["ArticulosPage"]
    }];

    var ArticulosPageRoutingModule = function ArticulosPageRoutingModule() {
      _classCallCheck(this, ArticulosPageRoutingModule);
    };

    ArticulosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ArticulosPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/articulos/articulos.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/articulos/articulos.module.ts ***!
    \*****************************************************/

  /*! exports provided: ArticulosPageModule */

  /***/
  function srcAppPagesArticulosArticulosModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArticulosPageModule", function () {
      return ArticulosPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _articulos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./articulos-routing.module */
    "./src/app/pages/articulos/articulos-routing.module.ts");
    /* harmony import */


    var _articulos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./articulos.page */
    "./src/app/pages/articulos/articulos.page.ts");

    var ArticulosPageModule = function ArticulosPageModule() {
      _classCallCheck(this, ArticulosPageModule);
    };

    ArticulosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _articulos_routing_module__WEBPACK_IMPORTED_MODULE_5__["ArticulosPageRoutingModule"]],
      declarations: [_articulos_page__WEBPACK_IMPORTED_MODULE_6__["ArticulosPage"]]
    })], ArticulosPageModule);
    /***/
  },

  /***/
  "./src/app/pages/articulos/articulos.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/pages/articulos/articulos.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesArticulosArticulosPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FydGljdWxvcy9hcnRpY3Vsb3MucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/articulos/articulos.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/articulos/articulos.page.ts ***!
    \***************************************************/

  /*! exports provided: ArticulosPage */

  /***/
  function srcAppPagesArticulosArticulosPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArticulosPage", function () {
      return ArticulosPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ArticulosPage = /*#__PURE__*/function () {
      function ArticulosPage() {
        _classCallCheck(this, ArticulosPage);
      }

      _createClass(ArticulosPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ArticulosPage;
    }();

    ArticulosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-articulos',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./articulos.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/articulos/articulos.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./articulos.page.scss */
      "./src/app/pages/articulos/articulos.page.scss"))["default"]]
    })], ArticulosPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-articulos-articulos-module-es5.js.map