function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-centrosde-apoyo-centrosde-apoyo-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesCentrosdeApoyoCentrosdeApoyoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>CentrosdeApoyo</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/centrosde-apoyo/centrosde-apoyo-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/pages/centrosde-apoyo/centrosde-apoyo-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: CentrosdeApoyoPageRoutingModule */

  /***/
  function srcAppPagesCentrosdeApoyoCentrosdeApoyoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CentrosdeApoyoPageRoutingModule", function () {
      return CentrosdeApoyoPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _centrosde_apoyo_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./centrosde-apoyo.page */
    "./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.ts");

    var routes = [{
      path: '',
      component: _centrosde_apoyo_page__WEBPACK_IMPORTED_MODULE_3__["CentrosdeApoyoPage"]
    }];

    var CentrosdeApoyoPageRoutingModule = function CentrosdeApoyoPageRoutingModule() {
      _classCallCheck(this, CentrosdeApoyoPageRoutingModule);
    };

    CentrosdeApoyoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CentrosdeApoyoPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/centrosde-apoyo/centrosde-apoyo.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/centrosde-apoyo/centrosde-apoyo.module.ts ***!
    \*****************************************************************/

  /*! exports provided: CentrosdeApoyoPageModule */

  /***/
  function srcAppPagesCentrosdeApoyoCentrosdeApoyoModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CentrosdeApoyoPageModule", function () {
      return CentrosdeApoyoPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _centrosde_apoyo_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./centrosde-apoyo-routing.module */
    "./src/app/pages/centrosde-apoyo/centrosde-apoyo-routing.module.ts");
    /* harmony import */


    var _centrosde_apoyo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./centrosde-apoyo.page */
    "./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.ts");

    var CentrosdeApoyoPageModule = function CentrosdeApoyoPageModule() {
      _classCallCheck(this, CentrosdeApoyoPageModule);
    };

    CentrosdeApoyoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _centrosde_apoyo_routing_module__WEBPACK_IMPORTED_MODULE_5__["CentrosdeApoyoPageRoutingModule"]],
      declarations: [_centrosde_apoyo_page__WEBPACK_IMPORTED_MODULE_6__["CentrosdeApoyoPage"]]
    })], CentrosdeApoyoPageModule);
    /***/
  },

  /***/
  "./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesCentrosdeApoyoCentrosdeApoyoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NlbnRyb3NkZS1hcG95by9jZW50cm9zZGUtYXBveW8ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.ts ***!
    \***************************************************************/

  /*! exports provided: CentrosdeApoyoPage */

  /***/
  function srcAppPagesCentrosdeApoyoCentrosdeApoyoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CentrosdeApoyoPage", function () {
      return CentrosdeApoyoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CentrosdeApoyoPage = /*#__PURE__*/function () {
      function CentrosdeApoyoPage() {
        _classCallCheck(this, CentrosdeApoyoPage);
      }

      _createClass(CentrosdeApoyoPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CentrosdeApoyoPage;
    }();

    CentrosdeApoyoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-centrosde-apoyo',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./centrosde-apoyo.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./centrosde-apoyo.page.scss */
      "./src/app/pages/centrosde-apoyo/centrosde-apoyo.page.scss"))["default"]]
    })], CentrosdeApoyoPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-centrosde-apoyo-centrosde-apoyo-module-es5.js.map