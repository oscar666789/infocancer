function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-casos-casos-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/casos/casos.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/casos/casos.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesCasosCasosPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>casos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/casos/casos-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/casos/casos-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: CasosPageRoutingModule */

  /***/
  function srcAppPagesCasosCasosRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CasosPageRoutingModule", function () {
      return CasosPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _casos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./casos.page */
    "./src/app/pages/casos/casos.page.ts");

    var routes = [{
      path: '',
      component: _casos_page__WEBPACK_IMPORTED_MODULE_3__["CasosPage"]
    }];

    var CasosPageRoutingModule = function CasosPageRoutingModule() {
      _classCallCheck(this, CasosPageRoutingModule);
    };

    CasosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CasosPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/casos/casos.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/casos/casos.module.ts ***!
    \*********************************************/

  /*! exports provided: CasosPageModule */

  /***/
  function srcAppPagesCasosCasosModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CasosPageModule", function () {
      return CasosPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _casos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./casos-routing.module */
    "./src/app/pages/casos/casos-routing.module.ts");
    /* harmony import */


    var _casos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./casos.page */
    "./src/app/pages/casos/casos.page.ts");

    var CasosPageModule = function CasosPageModule() {
      _classCallCheck(this, CasosPageModule);
    };

    CasosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _casos_routing_module__WEBPACK_IMPORTED_MODULE_5__["CasosPageRoutingModule"]],
      declarations: [_casos_page__WEBPACK_IMPORTED_MODULE_6__["CasosPage"]]
    })], CasosPageModule);
    /***/
  },

  /***/
  "./src/app/pages/casos/casos.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/pages/casos/casos.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesCasosCasosPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Nhc29zL2Nhc29zLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/pages/casos/casos.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/casos/casos.page.ts ***!
    \*******************************************/

  /*! exports provided: CasosPage */

  /***/
  function srcAppPagesCasosCasosPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CasosPage", function () {
      return CasosPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CasosPage = /*#__PURE__*/function () {
      function CasosPage() {
        _classCallCheck(this, CasosPage);
      }

      _createClass(CasosPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CasosPage;
    }();

    CasosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-casos',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./casos.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/casos/casos.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./casos.page.scss */
      "./src/app/pages/casos/casos.page.scss"))["default"]]
    })], CasosPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-casos-casos-module-es5.js.map