function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-cuentanostucaso-cuentanostucaso-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cuentanostucaso/cuentanostucaso.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cuentanostucaso/cuentanostucaso.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesCuentanostucasoCuentanostucasoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>cuentanostucaso</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/cuentanostucaso/cuentanostucaso-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/pages/cuentanostucaso/cuentanostucaso-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: CuentanostucasoPageRoutingModule */

  /***/
  function srcAppPagesCuentanostucasoCuentanostucasoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CuentanostucasoPageRoutingModule", function () {
      return CuentanostucasoPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _cuentanostucaso_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./cuentanostucaso.page */
    "./src/app/pages/cuentanostucaso/cuentanostucaso.page.ts");

    var routes = [{
      path: '',
      component: _cuentanostucaso_page__WEBPACK_IMPORTED_MODULE_3__["CuentanostucasoPage"]
    }];

    var CuentanostucasoPageRoutingModule = function CuentanostucasoPageRoutingModule() {
      _classCallCheck(this, CuentanostucasoPageRoutingModule);
    };

    CuentanostucasoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CuentanostucasoPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/cuentanostucaso/cuentanostucaso.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/cuentanostucaso/cuentanostucaso.module.ts ***!
    \*****************************************************************/

  /*! exports provided: CuentanostucasoPageModule */

  /***/
  function srcAppPagesCuentanostucasoCuentanostucasoModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CuentanostucasoPageModule", function () {
      return CuentanostucasoPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _cuentanostucaso_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./cuentanostucaso-routing.module */
    "./src/app/pages/cuentanostucaso/cuentanostucaso-routing.module.ts");
    /* harmony import */


    var _cuentanostucaso_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./cuentanostucaso.page */
    "./src/app/pages/cuentanostucaso/cuentanostucaso.page.ts");

    var CuentanostucasoPageModule = function CuentanostucasoPageModule() {
      _classCallCheck(this, CuentanostucasoPageModule);
    };

    CuentanostucasoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _cuentanostucaso_routing_module__WEBPACK_IMPORTED_MODULE_5__["CuentanostucasoPageRoutingModule"]],
      declarations: [_cuentanostucaso_page__WEBPACK_IMPORTED_MODULE_6__["CuentanostucasoPage"]]
    })], CuentanostucasoPageModule);
    /***/
  },

  /***/
  "./src/app/pages/cuentanostucaso/cuentanostucaso.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/cuentanostucaso/cuentanostucaso.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesCuentanostucasoCuentanostucasoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2N1ZW50YW5vc3R1Y2Fzby9jdWVudGFub3N0dWNhc28ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/cuentanostucaso/cuentanostucaso.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/cuentanostucaso/cuentanostucaso.page.ts ***!
    \***************************************************************/

  /*! exports provided: CuentanostucasoPage */

  /***/
  function srcAppPagesCuentanostucasoCuentanostucasoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CuentanostucasoPage", function () {
      return CuentanostucasoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CuentanostucasoPage = /*#__PURE__*/function () {
      function CuentanostucasoPage() {
        _classCallCheck(this, CuentanostucasoPage);
      }

      _createClass(CuentanostucasoPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CuentanostucasoPage;
    }();

    CuentanostucasoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cuentanostucaso',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./cuentanostucaso.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/cuentanostucaso/cuentanostucaso.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./cuentanostucaso.page.scss */
      "./src/app/pages/cuentanostucaso/cuentanostucaso.page.scss"))["default"]]
    })], CuentanostucasoPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-cuentanostucaso-cuentanostucaso-module-es5.js.map