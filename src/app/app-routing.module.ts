import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes,CanActivate  } from '@angular/router';

const routes: Routes = [
  
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  },{
    path: 'CrearCentros',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  
  {
    path: 'centrosde-apoyo',
    loadChildren: () => import('./pages/centrosde-apoyo/centrosde-apoyo.module').then( m => m.CentrosdeApoyoPageModule)
  },
  {
    path: 'articulos',
    loadChildren: () => import('./pages/articulos/articulos.module').then( m => m.ArticulosPageModule)
  },
  {
    path: 'cuentanostucaso',
    loadChildren: () => import('./pages/cuentanostucaso/cuentanostucaso.module').then( m => m.CuentanostucasoPageModule)
  },
  {
    path: 'casos',
    loadChildren: () => import('./pages/casos/casos.module').then( m => m.CasosPageModule)
  },

  
  {
    path: 'crear-articulos',
    loadChildren: () => import('./pages/crear-articulos/crear-articulos.module').then( m => m.CrearArticulosPageModule)
  },
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
