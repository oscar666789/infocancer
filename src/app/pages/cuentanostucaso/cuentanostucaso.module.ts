import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentanostucasoPageRoutingModule } from './cuentanostucaso-routing.module';

import { CuentanostucasoPage } from './cuentanostucaso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuentanostucasoPageRoutingModule
  ],
  declarations: [CuentanostucasoPage]
})
export class CuentanostucasoPageModule {}
