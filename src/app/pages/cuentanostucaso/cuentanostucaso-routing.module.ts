import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentanostucasoPage } from './cuentanostucaso.page';

const routes: Routes = [
  {
    path: '',
    component: CuentanostucasoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentanostucasoPageRoutingModule {}
