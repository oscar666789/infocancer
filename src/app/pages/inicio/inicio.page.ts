import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../servicios/auth.service';
interface Components {
  icon: string;
  name: string;
  redirectTo: string;
}
interface Components2 {
  icon: string;
  name: string;
  redirectTo: string;
}
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  components: Components[] = [
    {
      icon: "info",
      name: "Articulos",
      redirectTo: "../articulos",
    }, {
      icon: "home",
      name: "Centros de apoyo",
      redirectTo: "../centrosde-apoyo",
    },
    
    
    {
      icon: "file-tray-stacked-sharp",
      name: "Admin",
      redirectTo: "../login",
    },{
      icon: "female-sharp",
      name: "Crear Centros de apoyo",
      redirectTo: "../CrearCentros",
    },{
      icon: "female-sharp",
      name: "Crear Articulos",
      redirectTo: "../Crear-Articulos",
    },

  ];
  components2: Components2[] = [
    {
      icon: "female-sharp",
      name: "crear Centros de apoyo",
      redirectTo: "../CrearCentros",
    },
    {
      icon: "female-sharp",
      name: "Casos",
      redirectTo: "../Casos",
    },
  ];
  constructor(public authservice: AuthService) { }
  
	Onlogout() {
		this.authservice.logout();
  }
  

  ngOnInit() {
  }

}
