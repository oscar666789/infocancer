import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearArticulosPageRoutingModule } from './crear-articulos-routing.module';

import { CrearArticulosPage } from './crear-articulos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearArticulosPageRoutingModule
  ],
  declarations: [CrearArticulosPage]
})
export class CrearArticulosPageModule {}
