import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearArticulosPage } from './crear-articulos.page';

describe('CrearArticulosPage', () => {
  let component: CrearArticulosPage;
  let fixture: ComponentFixture<CrearArticulosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearArticulosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearArticulosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
