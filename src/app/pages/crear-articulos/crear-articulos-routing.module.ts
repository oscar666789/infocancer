import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearArticulosPage } from './crear-articulos.page';

const routes: Routes = [
  {
    path: '',
    component: CrearArticulosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearArticulosPageRoutingModule {}
