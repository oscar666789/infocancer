import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CentrosdeApoyoPageRoutingModule } from './centrosde-apoyo-routing.module';

import { CentrosdeApoyoPage } from './centrosde-apoyo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CentrosdeApoyoPageRoutingModule
  ],
  declarations: [CentrosdeApoyoPage]
})
export class CentrosdeApoyoPageModule {}
