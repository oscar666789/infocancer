import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CentrosdeApoyoPage } from './centrosde-apoyo.page';

const routes: Routes = [
  {
    path: '',
    component: CentrosdeApoyoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CentrosdeApoyoPageRoutingModule {}
