
import { Reporte } from './../../model/Reporte';
import { FirebaseService } from 'src/app/servicios/firebase.service';
import { Component, OnInit } from '@angular/core';
import * as Mapboxgl from 'mapbox-gl';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-centrosde-apoyo',
  templateUrl: './centrosde-apoyo.page.html',
  styleUrls: ['./centrosde-apoyo.page.scss'],
  
})
export class CentrosdeApoyoPage implements OnInit {
  lati:string;
  long:string;
  centros: Reporte[];
  constructor(private centrosService:FirebaseService) { }
  mapa: Mapboxgl.Map;
  ngOnInit() {
    this.centrosService.getReportes().subscribe(res =>this.centros=res);
    (Mapboxgl as any).accessToken =environment.mapboxKey;
    this.mapa = new Mapboxgl.Map({
      container: 'mapa-mapbox',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-86.8896912, 21.1559494],
      zoom: 13.22
      });
      
   

  }
creamarcador(){
  
}
  crearmarkador(lng:number,lat:number){

    const marker=new Mapboxgl.Marker({
      draggable:true}).setLngLat([lng,lat])
    .addTo(this.mapa);


  }
 
  
    }

