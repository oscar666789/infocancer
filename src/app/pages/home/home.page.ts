import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import * as Mapboxgl from 'mapbox-gl';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
 mapa: Mapboxgl.Map;
  ngOnInit(){
    (Mapboxgl as any).accessToken =environment.mapboxKey;
    this.mapa = new Mapboxgl.Map({
      container: 'mapa-mapbox',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-86.8896912, 21.1559494],
      zoom: 13.22
      });
                 this.crearmarkador(-86.8896912, 21.1559494);
  }

  crearmarkador(lng:number,lat:number){

    const marker=new Mapboxgl.Marker({
      draggable:true}).setLngLat([lng,lat])
    .addTo(this.mapa);

     marker.on('drag',() =>{
       console.log(marker.getLngLat());

     })

  }
}
